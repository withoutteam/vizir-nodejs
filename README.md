# API - Telzir

API criada em ***NodeJs*** com ***Typescript*** para o processo seletivo da Vizir.

## Execução do projeto:

Após clonar o repositório, instale todas as dependencias com o comando `npm install` ou `yarn`.

#### Modo DESENVOLVIMENTO:
Para executar a API, deve rodar com o comando `npm run dev` ou `yarn dev`. O mesmo irá conectar com uma database em cloud já com dados cadastrados.

#### Modo TESTES:
Em modo teste, deve-se primeiramente executar com o comando `npm run dev-test` ou `yarn dev-test` para conectar-se com a database de testes. Mantenha em execução, e em outra aba/janela/terminal execute o comando `npm test` ou `yarn test`. Se deparar com essa mensagem:

```
No tests found related to files changed since last commit.
Press `a` to run all tests, or run Jest with `--watchAll`.

Watch Usage
 › Press a to run all tests.
 › Press f to run only failed tests.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press q to quit watch mode.
 › Press Enter to trigger a test run.
```

 Pressione **a** para executar todos os testes.

#### Modo PRODUÇÃO:
Para executar neste modo, deve realizar o processo de transpiling do Typescript para Javascript, para isso execute o comando `npm run build` ou `yarn build` e aguarde. Se não existir, irá criar uma pasta chamada **dist**.
Após isso, execute o comando `npm start` ou `yarn start` para rodar diretamente do Javascript em modo *production*.

## ROTAS

#### GET:
`/tabela-preco` : Consulta toda a tabela de preço.

`/tabela-preco/:id` : Consulta um item só, através de seu ID.

#### POST:
`/tabela-preco` : Criar um novo item na tabela de preço.

**JSON esperado:**
```JSON
{
	"dddOrigem": 11,
	"dddDestino": 16,
	"valorTarifa": 1.90
}
```

**JSON de resposta esperado:**
```JSON
{
  "erro": false,
  "conteudo": {
    "dddOrigem": 11,
    "dddDestino": 16,
    "valorTarifa": 1.9,
    "criadoEm": "2019-08-16T15:59:16.225Z",
    "alteradoEm": "2019-08-16T15:59:16.225Z",
    "_id": "a0ac0b8a-73cf-4792-b74d-8a7ab2827b20",
    "__v": 0
  }
}
```

`/valor-chamada` : Rota para calcular o preço de simulação com o plano.

**JSON esperado:**
```JSON
{
  "dddOrigem": 18,
  "dddDestino": 11,
  "tempo": 200,
  "plano": 120
}
```
**JSON de resposta esperado:**
```JSON
{
  "error": false,
  "conteudo": {
    "comFaleMais": "167.20",
    "semFaleMais": "380.00"
  }
}
```

#### PUT:
`/tabela-preco/:id` : Atualiza item da tabela de preço atráves de seu ID.

**JSON esperado:**
```JSON
{
  "dddOrigem": 11,
  "dddDestino": 16,
  "valorTarifa": 0.2
}
```

**JSON de resposta esperado:**
```JSON
{
  "erro": false,
  "conteudo": {
    "_id": "23dee5d5-2151-4df3-91c5-9b872e824aab",
    "dddOrigem": 11,
    "dddDestino": 16,
    "valorTarifa": 0.2,
    "criadoEm": "2019-08-14T01:33:43.055Z",
    "alteradoEm": "2019-08-16T16:10:58.929Z",
    "__v": 0
  }
}
```

#### DELETE
`/tabela-preco/:id`: Deleta um item da tabela pelo seu ID.
