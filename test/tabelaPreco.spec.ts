import axios from 'axios'
import listaTabelaPreco from './../src/utils/TabelaPrecos'
import TabelaPrecoInterface from './../src/interfaces/TabelaPrecoInterface'
const uri = 'http://localhost:3333/'

let listaItens: TabelaPrecoInterface[]

describe('CADASTRO', (): void => {
  it('Cadastrar todos os valores da tabela de preços', async (): Promise<void> => {
    for (let i in listaTabelaPreco) {
      await axios.post(`${uri}tabela-preco`, listaTabelaPreco[i])
        .then((res): void => {
          expect(res.status).toEqual(200)
        })
    }
  })
})

describe('CONSULTAR', (): void => {
  it('Deve consultar todas as tabelas de preco', async (): Promise<void> => {
    await axios.get(`${uri}tabela-preco`)
      .then((res): void => {
        listaItens = res.data

        expect(res.status).toEqual(200)
        expect(res.data.erro).toEqual(false)
        expect(res.data.conteudo.length).toBeGreaterThan(0)
      })
  })

  it('Deve consultar item pelo seu ID', async (): Promise<void> => {
    for (let i in listaItens) {
      await axios.get(`${uri}tabela-preco/${listaItens[i]._id}`)
        .then((res): void => {
          expect(res.status).toEqual(200)
        })
    }
  })
})

describe('CALCULO', (): void => {
  it('Deve retornar um objeto informando o valor zerado quando dentro do limite do plano', async (): Promise<void> => {
    const objDentroPlano = {
      dddOrigem: 18,
      dddDestino: 11,
      tempo: 100,
      plano: 120
    }

    await axios.post(`${uri}valor-chamada`, objDentroPlano).then((res): void => {
      expect.objectContaining({
        error: expect.any(Boolean),
        valorTotal: expect.any(Number)
      })

      expect(res.data.error).toEqual(false)
      expect(res.data.conteudo.comFaleMais).toEqual('0.00')
      expect(parseInt(res.data.conteudo.semFaleMais)).toBeGreaterThan(0)
    })
  })

  it('Deve retornar um objeto informando o valor acima de zero informando a taxa a ser paga', async (): Promise<void> => {
    const objAcimaPlano = {
      dddOrigem: 18,
      dddDestino: 11,
      tempo: 200,
      plano: 120
    }

    await axios.post(`${uri}valor-chamada`, objAcimaPlano).then((res): void => {
      expect.objectContaining({
        error: expect.any(Boolean),
        valorTotal: expect.any(Number)
      })

      expect(res.data.error).toEqual(false)
      expect(parseInt(res.data.conteudo.semFaleMais)).toBeGreaterThan(0)
    })
  })

  it('Deve retornar um objeto com uma mensagem informando que o DDD de origem e o de destino não é coberto pelo plano',
    async (): Promise<void> => {
      const objPlanoInvalido = {
        dddOrigem: 18,
        dddDestino: 17,
        tempo: 100,
        plano: 120
      }

      const { response, isAxiosError } = await axios.post(`${uri}valor-chamada`, objPlanoInvalido)
        .catch((err): any => ({ ...err }))

      expect(response.status).toEqual(400)
      expect(isAxiosError).toEqual(true)

      expect.objectContaining({
        error: expect.any(Boolean),
        messageError: expect.any(String)
      })

      expect(response.data.error).toEqual(true)
      expect(response.data.conteudo.messageError).toEqual('Seu plano não atende à esse destino e origem')
    })
})

describe('ATUALIZAR', (): void => {
  it('Deve alterar cada item identificando pelo ID', async (): Promise<void> => {
    const valorTarifaRandom = (): number => Number((Math.random() * 10).toFixed(2))
    const dddRandom = (): number => Number((Math.random() * 100).toFixed(0))

    for (let i in listaItens) {
      await axios.put(`${uri}tabela-preco/${listaItens[i]._id}`, {
        dddOrigem: dddRandom(),
        dddDestino: dddRandom(),
        valorTarifa: valorTarifaRandom()
      })
        .then((res): void => {
          expect(res.status).toEqual(200)
          expect(res.data.erro).toEqual(false)
          expect(res.data.conteudo).not.toBeTruthy()
        })
    }
  })
})

describe('DELETAR', (): void => {
  it('Deve ser possível deletar um item pelo seu ID', async (): Promise<void> => {
    await axios.get(`${uri}tabela-preco`)
      .then(async (res): Promise<void> => {
        for (let i in res.data.conteudo) {
          await axios.delete(`${uri}tabela-preco/${res.data.conteudo[i]._id}`)
            .then((res): void => {
              expect(res.status).toEqual(200)
              expect(res.data.conteudo).not.toBeNull()
            })
        }
      })
  })
})
