import { Document } from 'mongoose'

export default interface TabelaPrecoInterface extends Document {
  _id: string
  dddOrigem: number
  dddDestino: number
  valorTarifa: number
}
