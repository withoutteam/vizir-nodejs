import { Schema, model, models } from 'mongoose'
import TabelaPrecoInterface from './../interfaces/TabelaPrecoInterface'
import { v4 } from 'uuid'

const TabelaPrecoSchema = new Schema(
  {
    _id: {
      type: String
    },
    dddOrigem: {
      type: Number,
      required: true
    },
    dddDestino: {
      type: Number,
      required: true
    },
    valorTarifa: {
      type: Number,
      required: true
    }
  },
  {
    timestamps: {
      createdAt: 'criadoEm',
      updatedAt: 'alteradoEm'
    }
  }
)

TabelaPrecoSchema.pre<TabelaPrecoInterface>('save', function (): void {
  this._id = v4()
})

export default models['tabela-preco'] || model<TabelaPrecoInterface>('tabela-preco', TabelaPrecoSchema)
