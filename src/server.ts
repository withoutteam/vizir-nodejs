import app from './app'
const port = process.env.PORT || 3333

app.listen(port, (): void =>
  console.log(`  APP rodando no endereço http://localhost:${port}.
  Ctrl+ C para interromper a execução.
  `)
)
