import CalculadoraPrecoController from './CalculadoraPrecoController'
import { Request, Response } from 'express'
import ResponseHandingInterface from '../interfaces/ResponseHandingInterface'

export default class ChamadaController extends CalculadoraPrecoController {
  public async consultaValorChamada (
    req: Request,
    res: Response
  ): Promise<Response> {
    const { dddOrigem, dddDestino, tempo, plano } = req.body

    const resposta: ResponseHandingInterface = await this.calcularPreco(
      dddOrigem,
      dddDestino,
      tempo,
      plano
    )

    return resposta.error
      ? res.status(400).json(resposta)
      : res.status(200).json(resposta)
  }
}
