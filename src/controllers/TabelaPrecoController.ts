import { Request, Response } from 'express'
import TabelaPrecoModel from './../model/TabelaDePrecoModel'
import TabelaPrecoInterface from './../interfaces/TabelaPrecoInterface'
import { RespostaRequisicao } from './../utils/RespostaRequisicao'

export default class TabelaPrecoController {
  public async salvaNovoPreco (req: Request, res: Response): Promise<Response> {
    return TabelaPrecoModel.create(req.body)
      .then((tabelaCriada): Response => RespostaRequisicao(false, tabelaCriada, 200, res))
      .catch((err): Response => RespostaRequisicao(true, err, 400, res))
  }

  public async consultaTabelaPrecoPorID (req: Request, res: Response): Promise<Response> {
    return TabelaPrecoModel.findById(req.params.id)
      .then((tabelaEcontrada): Response => RespostaRequisicao(false, tabelaEcontrada, 200, res))
      .catch((err): Response => RespostaRequisicao(true, err, 400, res))
  }

  public async consultaTodasTabelasPreco (req: Request, res: Response): Promise<Response> {
    return TabelaPrecoModel.find()
      .then((listaPrecos: TabelaPrecoInterface[]): Response => {
        const listaValoresFormatados = listaPrecos.map((item): object => ({
          _id: item._id,
          dddOrigem: item.dddOrigem,
          dddDestino: item.dddDestino,
          valorTarifa: String(item.valorTarifa.toFixed(2))
        }))
        return RespostaRequisicao(false, listaValoresFormatados, 200, res)
      })
      .catch((err): Response => RespostaRequisicao(true, err, 400, res))
  }

  public async deletaTabelaPrecoPorID (req: Request, res: Response): Promise<Response> {
    return TabelaPrecoModel.findByIdAndDelete(req.params.id)
      .then((itemDeletado): Response => {
        return itemDeletado
          ? RespostaRequisicao(false, 'Item deletado com sucesso', 200, res)
          : RespostaRequisicao(true, 'Item não encontrado.', 400, res)
      })
      .catch((err): Response => RespostaRequisicao(true, err, 400, res))
  }

  public async alteraTabelaPrecoPorID (req: Request, res: Response): Promise<Response> {
    return TabelaPrecoModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
      .then((itemAtualizado): Response => RespostaRequisicao(false, itemAtualizado, 200, res))
      .catch((err): Response => RespostaRequisicao(true, err, 400, res))
  }
}
