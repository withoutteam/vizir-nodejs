import TabelaPrecoModel from './../model/TabelaDePrecoModel'
import ResponseHandingInterface from '../interfaces/ResponseHandingInterface'

export default class CalculadoraPrecoController {
  protected async verificaValorPlano (
    dddOrigem: number,
    dddDestino: number
  ): Promise<number> {
    const tabelaPrecos = await TabelaPrecoModel.find()
    return new Promise((resolve, reject): void => {
      const preco = tabelaPrecos.find(
        (item): boolean =>
          item.dddDestino === dddDestino && item.dddOrigem === dddOrigem
      )

      !preco
        ? reject(
          new Error('Seu plano não atende à esse destino e origem').message
        )
        : resolve(preco.valorTarifa)
    })
  }

  protected async calcularPreco (
    dddOrigem: number,
    dddDestino: number,
    tempo: number,
    plano: number
  ): Promise<ResponseHandingInterface> {
    return this.verificaValorPlano(dddOrigem, dddDestino)
      .then(
        (preco: number): ResponseHandingInterface => {
          const comFaleMais = this.calculaComFaleMais(tempo, plano, preco)
          const semFaleMais = this.calculaSemFaleMais(tempo, preco)

          return {
            error: false,
            conteudo: {
              comFaleMais,
              semFaleMais
            }
          }
        }
      )
      .catch(
        (messageError: string): ResponseHandingInterface => ({
          error: true,
          conteudo: {
            messageError
          }
        })
      )
  }

  private calculaComFaleMais (tempo: number, plano: number, preco: number): string {
    const minutosExcedentes = tempo - plano
    const valorExtra = minutosExcedentes * preco
    let valorTotal = valorExtra * 0.1 + valorExtra

    if (valorTotal < 0) valorTotal = 0

    return String(valorTotal.toFixed(2))
  }

  private calculaSemFaleMais (tempo: number, preco: number): string {
    const valorTotal = tempo * preco

    return String(valorTotal.toFixed(2))
  }
}
