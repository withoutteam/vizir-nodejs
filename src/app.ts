import express from 'express'
import cors from 'cors'
import routes from './routes/index'
import VariaveisAmbiente from './utils/VariaveisAmbiente'
import ConexaoDB from './config/conexaoDB'
class App {
  public express: express.Application;

  public constructor () {
    this.express = express()

    this.middlewares()
    this.routes()
    this.criaVariaveisAmbiente()
    this.realizaConexaoDB()
  }

  private middlewares (): void {
    this.express.use(express.json())
    this.express.use(cors())
  }

  private realizaConexaoDB (): void {
    ConexaoDB()
  }

  private routes (): void {
    this.express.use(routes)
  }

  private criaVariaveisAmbiente (): void {
    VariaveisAmbiente()
  }
}

export default new App().express
