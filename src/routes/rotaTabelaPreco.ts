import { Router, Request, Response } from 'express'
import TabelaPrecoController from '../controllers/TabelaPrecoController'

const tabelaPrecoController = new TabelaPrecoController()

export default function (routes: Router): void {
  routes.get('/tabela-preco', (req: Request, res: Response): Promise<Response> =>
    tabelaPrecoController.consultaTodasTabelasPreco(req, res)
  )

  routes.get('/tabela-preco/:id', (req: Request, res: Response): Promise<Response> =>
    tabelaPrecoController.consultaTabelaPrecoPorID(req, res)
  )

  routes.post('/tabela-preco', (req: Request, res: Response): Promise<Response> =>
    tabelaPrecoController.salvaNovoPreco(req, res)
  )

  routes.delete('/tabela-preco/:id', (req: Request, res: Response): Promise<Response> =>
    tabelaPrecoController.deletaTabelaPrecoPorID(req, res)
  )

  routes.put('/tabela-preco/:id', (req: Request, res: Response): Promise<Response> =>
    tabelaPrecoController.alteraTabelaPrecoPorID(req, res)
  )
}
