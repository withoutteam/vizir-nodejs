import { Router, Request, Response } from 'express'
import rotaCalculoChamada from './rotaCalculoChamada'
import rotaTabelaPreco from './rotaTabelaPreco'

const routes = Router()

routes.get('/', (req: Request, res: Response): Response => res.status(200).json('Api TelZir'))

rotaCalculoChamada(routes)
rotaTabelaPreco(routes)

export default routes
