import { Router, Request, Response } from 'express'
import ChamadaController from '../controllers/ChamadaController'

const chamadaController = new ChamadaController()

export default function (routes: Router): void {
  routes.post('/valor-chamada', async (req: Request, res: Response): Promise<Response> =>
    chamadaController.consultaValorChamada(req, res)
  )
}
