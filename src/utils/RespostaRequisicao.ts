import { Response } from 'express'

export function RespostaRequisicao (condicaoErro: boolean, mensagemConteudo: any, codigoStatus: number, res: Response): Response {
  return res.status(codigoStatus).json({
    erro: condicaoErro,
    conteudo: mensagemConteudo
  })
}
