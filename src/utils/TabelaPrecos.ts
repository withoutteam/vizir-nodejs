export default [
  {
    dddOrigem: 11,
    dddDestino: 16,
    valorTarifa: 1.90
  }, {
    dddOrigem: 16,
    dddDestino: 11,
    valorTarifa: 2.90
  }, {
    dddOrigem: 11,
    dddDestino: 17,
    valorTarifa: 1.70
  }, {
    dddOrigem: 17,
    dddDestino: 11,
    valorTarifa: 2.70
  }, {
    dddOrigem: 11,
    dddDestino: 18,
    valorTarifa: 0.90
  }, {
    dddOrigem: 18,
    dddDestino: 11,
    valorTarifa: 1.90
  }
]
