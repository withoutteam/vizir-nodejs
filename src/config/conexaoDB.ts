import mongoose from 'mongoose'
let db: any
const dbDEV = 'telzir-dev'
const dbTEST = 'telzir-test'
const dbPROD = 'telzir-prod'

function verifiaModoExecucao (database: string): void {
  if (!db) {
    db = mongoose.connect(`mongodb+srv://omnistack:omnistack@cluster0-6tvwh.mongodb.net/${database}?retryWrites=true&w=majority`, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    })
      .then((): void => console.log(`Conectado em modo ${String(process.env.NODE_ENV).toUpperCase()} na database ${database}`))
  }
}

export default function ConexaoDB (): mongoose.Connection {
  switch (process.env.NODE_ENV) {
    case 'development':
      verifiaModoExecucao(dbDEV)
      break
    case 'test':
      verifiaModoExecucao(dbTEST)
      break
    case 'production':
      verifiaModoExecucao(dbPROD)
      break
    default:
      verifiaModoExecucao(dbPROD)
  }
  return db
}
